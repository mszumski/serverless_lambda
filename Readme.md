# Serverless hello test 

  - Install serverless

  - `serverless deploy`

## Calling API

```
URL=https://bwanokhuqc.execute-api.ap-southeast-2.amazonaws.com/dev/say/hello
API_KEY=****************
curl -s -H "x-api-key: $API_KEY" "$URL"
```


## Python packages installation 

```
docker run --rm -v $PWD:/data -w /data -it python:3.8 pip install -t . requests
```
